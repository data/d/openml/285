# OpenML dataset: flags

https://www.openml.org/d/285

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Richard S. Forsyth  
**Source**: Unknown - 5/15/1990  
**Please cite**:   

ARFF version of UCI dataset 'flags'.

Creators: Collected primarily from the "Collins Gem Guide to Flags": Collins Publishers (1986). Donor: Richard S. Forsyth. Date 5/15/1990

This data file contains details of various nations and their flags.
With this data you can try things like predicting the religion of a country from its size and the colours in its flag. 10 attributes are numeric-valued.  The remainder are either Boolean-  or nominal-valued.

Number of Instances: 194. Number of attributes: 30 (overall). Missing values: none

Attribute Information:
1. name Name of the country concerned
2. landmass 1=N.America, 2=S.America, 3=Europe, 4=Africa, 4=Asia, 6=Oceania
3. zone Geographic quadrant, based on Greenwich and the Equator 1=NE, 2=SE, 3=SW, 4=NW
4. area in thousands of square km
5. population in round millions
6. language 1=English, 2=Spanish, 3=French, 4=German, 5=Slavic, 6=Other Indo-European, 7=Chinese, 8=Arabic, 9=Japanese/Turkish/Finnish/Magyar, 10=Others
7. religion 0=Catholic, 1=Other Christian, 2=Muslim, 3=Buddhist, 4=Hindu, 5=Ethnic, 6=Marxist, 7=Others
8. bars     Number of vertical bars in the flag
9. stripes  Number of horizontal stripes in the flag
10. colours  Number of different colours in the flag
11. red      0 if red absent, 1 if red present in the flag
12. green    same for green
13. blue     same for blue
14. gold     same for gold (also yellow)
15. white    same for white
16. black    same for black
17. orange   same for orange (also brown)
18. mainhue  predominant colour in the flag (tie-breaks decided by taking the topmost hue, if that fails then the most central hue, and if that fails the leftmost hue)
19. circles  Number of circles in the flag
20. crosses  Number of (upright) crosses
21. saltires Number of diagonal crosses
22. quarters Number of quartered sections
23. sunstars Number of sun or star symbols
24. crescent 1 if a crescent moon symbol present, else 0
25. triangle 1 if any triangles present, 0 otherwise
26. icon     1 if an inanimate image present (e.g., a boat), otherwise 0
27. animate  1 if an animate image (e.g., an eagle, a tree, a human hand) present, 0 otherwise
28. text     1 if any letters or writing on the flag (e.g., a motto or slogan), 0 otherwise
29. topleft  colour in the top-left corner (moving right to decide tie-breaks)
30. botright Colour in the bottom-left corner (moving left to decide 
tie-breaks)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/285) of an [OpenML dataset](https://www.openml.org/d/285). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/285/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/285/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/285/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

